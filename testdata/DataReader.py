import openpyxl


class DataReader:

    @staticmethod
    def read_test_data():
        book = openpyxl.load_workbook("..\\testData\\DataSheet.xlsx")
        sheet = book.active
        return sheet

    @staticmethod
    def get_test_data(scenarioid):
        print(scenarioid)
        book = openpyxl.load_workbook("..\\testData\\DataSheet.xlsx")
        sheet = book.active
        datadict = {}
        datadictlist = []
        for i in range(1, sheet.max_row + 1):
            if sheet.cell(row=i, column=1).value == scenarioid:
                for j in range(2, sheet.max_column + 1):
                    datadict[sheet.cell(row=1, column=j).value] = sheet.cell(row=i, column=j).value
                datadictlist.append(datadict.copy())
        print(datadictlist)
        return datadictlist
