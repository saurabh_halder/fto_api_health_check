import json


class SwaggerReader:

    @staticmethod
    def read_product_v1_items_itemnumber_swagger():
        with open("..\\swagger\\get_product_v1_items_itemNumber.json") as swaggerreader:
            data = json.load(swaggerreader)
        return data

    @staticmethod
    def read_price_v2_item_price_swagger():
        with open("..\\swagger\\get_price_v2_item_price.json") as swaggerreader:
            data = json.load(swaggerreader)
        return data

    @staticmethod
    def read_price_from_barcode_swagger():
        with open("..\\swagger\\get_price_from_barcode.json") as swaggerreader:
            data = json.load(swaggerreader)
        return data

    @staticmethod
    def read_identity_token_swagger():
        with open("..\\swagger\\post_identity_token.json") as swaggerreader:
            data = json.load(swaggerreader)
        return data
