import json


class ConfigReader:

    @staticmethod
    def read_config():
        with open("..\\helpers\\apiconfig.json") as configreader:
            data = json.load(configreader)
        return data
