##### Price V2 Api Tests #####

from helpers.Logger import Logger
from testdata.DataReader import DataReader
from helpers.ConfigReader import ConfigReader
from helpers.SwaggerReader import SwaggerReader
import pytest
import requests
import json


class TestPriceV2(Logger):

    # Price V2 Response Code Test
    def test_Price_V2_GET_item_price_response_code(self, get_data_price_v2_get_item_price_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: GET/locations/{locationid}/items/{MIN}")
        apirequest = TestPriceV2.request_generator_item_price(get_data_price_v2_get_item_price_response)
        log.info("Api Request URL: " + apirequest[4])
        log.info("Testing with MIN: " + apirequest[2] + " and Store Id: " + apirequest[3])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        log.info("Test Completed")

    # Price V2 Response Content Test
    def test_Price_V2_GET_item_price_response_content(self, get_data_price_v2_get_item_price_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: GET/locations/{locationid}/items/{MIN}")
        apirequest = TestPriceV2.request_generator_item_price(get_data_price_v2_get_item_price_response)
        log.info("Api Request URL: " + apirequest[4])
        log.info("Testing with MIN: " + apirequest[2] + " and Store Id: " + apirequest[3])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        response_json = json.loads(response.text)
        diff = set(SwaggerReader.read_price_v2_item_price_swagger().keys()) - set(response_json.keys())
        # assert not diff, ("Response Body Mismatch with Swagger", "Test Failed")
        # log.info("Response Body Format is same as Swagger Definition")
        assert response_json, "JSON Response is Null"
        assert str(response_json['price']['locationId']) == apirequest[3], "Location Id is not matching with Input Store"
        assert response_json['price']['MIN'] == apirequest[2], "Product in JSON Response is not matching with Input MIN"
        assert response_json['price']['regularPrice'] != 0, "Price in JSON Response is 0"
        assert response_json['price']['sellingUnitOfMeasure'], "Selling Unit of Measure is Null"
        assert response_json['price']['effectiveDate'], "Effective Date is Null"
        log.info("JSON Response Fields are correct")
        log.info("Test Completed")

    @staticmethod
    def request_generator_item_price(get_data_price_v2_get_item_price_response):
        req_gen = []
        env = get_data_price_v2_get_item_price_response['Env']
        ProductId = get_data_price_v2_get_item_price_response['ProductId']
        StoreId = get_data_price_v2_get_item_price_response['StoreId']
        temp_api_url = get_data_price_v2_get_item_price_response['ApiUrl']
        api_url = temp_api_url.replace("<env>", env)
        api_url = api_url.replace("<store>", StoreId)
        api_url = api_url.replace("<MIN>", ProductId)
        api_key = ConfigReader.read_config()['pricev2_apikey']
        api_auth = ConfigReader.read_config()['pricev2_apiauth']
        requestUrl = api_url + api_key
        params = {"Authorization": api_auth}
        req_gen.extend([requestUrl, params, ProductId, StoreId, api_url])
        return req_gen

    @pytest.fixture(params=DataReader.get_test_data("PriceV2_001"))
    def get_data_price_v2_get_item_price_response(self, request):
        return request.param

    # @pytest.fixture(params=DataReader.get_test_data("PriceV2_002"))
    # def get_data_price_v2_get_item_price_response_content(self, request):
    #     return request.param
