##### Slot Bff Api Tests #####

from helpers.Logger import Logger
from testdata.DataReader import DataReader
from helpers.ConfigReader import ConfigReader
import pytest
import requests


class TestSlotBff(Logger):

    # Slotbff Response Code Test
    def test_slotbff_get_slots_response(self, get_data_get_slots_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: GET/fto/slotbff/v1/stores/")
        apirequest = TestSlotBff.request_generator(get_data_get_slots_response)
        log.info("Api Request URL: " + apirequest[4])
        log.info("Testing with Store Id: " + apirequest[2] + " and Slot Date: " + apirequest[3])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        log.info("Test Completed")

    @staticmethod
    def request_generator(get_data_get_slots_response):
        req_gen = []
        env = get_data_get_slots_response['Env']
        StoreId = get_data_get_slots_response['StoreId']
        Slotdate = get_data_get_slots_response['Slotdate']
        temp_api_url = get_data_get_slots_response['ApiUrl']
        api_url = temp_api_url.replace("<env>", env)
        api_url = api_url.replace("<store>", StoreId)
        api_url = api_url.replace("<slotdate>", Slotdate)
        api_key = ConfigReader.read_config()['slotbff_apikey']
        api_auth = ConfigReader.read_config()['slotbff_apiauth']
        requestUrl = api_url + api_key
        params = {"Authorization": api_auth}
        req_gen.extend([requestUrl, params, StoreId, Slotdate, api_url])
        return req_gen

    @pytest.fixture(params=DataReader.get_test_data("SlotBff_001"))
    def get_data_get_slots_response(self, request):
        return request.param
