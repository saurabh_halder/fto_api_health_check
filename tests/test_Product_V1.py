##### Product V1 Api Tests #####

from helpers.Logger import Logger
from testdata.DataReader import DataReader
from helpers.ConfigReader import ConfigReader
from helpers.SwaggerReader import SwaggerReader
import pytest
import requests
import json


class TestProductV1(Logger):

    # Product V1 Get Item Response Code Test
    def test_Product_V1_GET_items_itemnumber_response_code(self, get_data_product_v1_get_items_itemnumber_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: GET/product/v1/items/{itemNumber}")
        apirequest = TestProductV1.request_generator(get_data_product_v1_get_items_itemnumber_response)
        log.info("Api Request URL: " + apirequest[3])
        log.info("Testing with MIN: " + apirequest[2])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        log.info("Test Completed")

    # Product V1 Response Content Test
    def test_Product_V1_GET_items_itemnumber_response_content(self, get_data_product_v1_get_items_itemnumber_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: GET/product/v1/items/{itemNumber}")
        apirequest = TestProductV1.request_generator(get_data_product_v1_get_items_itemnumber_response)
        log.info("Api Request URL: " + apirequest[3])
        log.info("Testing with MIN: " + apirequest[2])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        response_json = json.loads(response.text)
        diff = set(SwaggerReader.read_product_v1_items_itemnumber_swagger().keys()) - set(response_json.keys())
        # assert not diff, ("Response Body Mismatch with Swagger", "Test Failed")
        # log.info("Response Body Format is same as Swagger Definition")
        assert response_json, "JSON Response is Null"
        assert response_json["itemNumber"] == apirequest[2]
        log.info("Input Product present in the JSON Response")
        assert response_json["gtins"], "Barcode is null in JSON Response"
        assert response_json["customerFriendlyDescription"], "Customer Friendly Description is null in JSON Response"
        assert response_json["itemDescription"], "Item Description is null in JSON Response"
        assert response_json["commercialHierarchy"], "Commercial Hierarchy is null in JSON Response"
        assert response_json["status"], "Status is null in JSON Response"
        assert response_json["sellingUnitOfMeasure"], "Selling Unit Of Measure is null in JSON Response"
        assert response_json["commercialHierarchy"]["department"], "Department is null in JSON Response"
        assert response_json["commercialHierarchy"]["departmentName"], "Department Name is null in JSON Response"
        log.info("JSON Fields - Gtins, Customer Friendly Description, Item Description, Status, Selling UOM, "
                 "Commercial Hierarchy,Department and Department Name are not empty")
        log.info("Test Completed")

    # Product V1 Search Response Code Test
    def test_Product_V1_POST_items_search_response_code(self, get_data_product_v1_post_items_search_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: POST//product/v1/items/@search")
        payload = TestProductV1.payload_generator(get_data_product_v1_post_items_search_response)
        log.info("Api Request URL: " + payload[2])
        response = requests.post(payload[0], headers=payload[1])
        log.info("Api POST Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        log.info("Test Completed")

    @staticmethod
    def request_generator(get_data_product_v1_get_items_itemnumber_response):
        req_gen = []
        env = get_data_product_v1_get_items_itemnumber_response['Env']
        ProductId = get_data_product_v1_get_items_itemnumber_response['ProductId']
        temp_api_url = get_data_product_v1_get_items_itemnumber_response['ApiUrl']
        api_url = temp_api_url.replace("<env>", env)
        api_url = api_url.replace("<MIN>", ProductId)
        api_key = ConfigReader.read_config()['productv1_apikey']
        api_auth = ConfigReader.read_config()['productv1_apiauth']
        requestUrl = api_url + api_key
        params = {"Authorization": api_auth}
        req_gen.extend([requestUrl, params, ProductId, api_url])
        return req_gen

    @staticmethod
    def payload_generator(get_data_product_v1_post_items_search_response):
        req_gen = []
        env = get_data_product_v1_post_items_search_response['Env']
        temp_api_url = get_data_product_v1_post_items_search_response['ApiUrl']
        api_url = temp_api_url.replace("<env>", env)
        api_key = ConfigReader.read_config()['productv1_apikey']
        api_auth = ConfigReader.read_config()['productv1_apiauth']
        requestUrl = api_url + api_key + "&filter_path=hits.hits._source"
        params = {"Authorization": api_auth}
        req_gen.extend([requestUrl, params, api_url])
        return req_gen

    @pytest.fixture(params=DataReader.get_test_data("ProductV1_001"))
    def get_data_product_v1_get_items_itemnumber_response(self, request):
        return request.param

    # @pytest.fixture(params=DataReader.get_test_data("ProductV1_002"))
    # def get_data_product_v1_get_items_itemnumber_response_content(self, request):
    #     return request.param

    @pytest.fixture(params=DataReader.get_test_data("ProductV1_003"))
    def get_data_product_v1_post_items_search_response(self, request):
        return request.param
