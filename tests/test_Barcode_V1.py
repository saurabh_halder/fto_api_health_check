##### Barcode V1 Api Tests #####

from helpers.Logger import Logger
from testdata.DataReader import DataReader
from helpers.ConfigReader import ConfigReader
from helpers.SwaggerReader import SwaggerReader
import pytest
import requests
import json


class TestBarcodeV1(Logger):

    # Barcode V1 Response Code Test
    def test_barcode_v1_get_item_price_from_barcode_response_code(self, get_data_barcode_v1_get_item_price_from_barcode_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: GET/barcode/v1/1d/{barcode}/price")
        apirequest = TestBarcodeV1.request_generator(get_data_barcode_v1_get_item_price_from_barcode_response)
        log.info("Api Request URL: " + apirequest[3])
        log.info("Testing with Barcode: " + apirequest[2])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        log.info("Test Completed")

    # Barcode V1 Response Content Test
    def test_barcode_v1_get_item_price_from_barcode_response_content(self, get_data_barcode_v1_get_item_price_from_barcode_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: GET/barcode/v1/1d/{barcode}/price")
        apirequest = TestBarcodeV1.request_generator(get_data_barcode_v1_get_item_price_from_barcode_response)
        log.info("Api Request URL: " + apirequest[3])
        log.info("Testing with Barcode: " + apirequest[2])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        response_json = json.loads(response.text)
        assert response_json, "JSON Response is Null"
        assert response_json["price"], "Price in JSON Response is Null"
        log.info("JSON Fields Validation Performed")
        log.info("Test Completed")

    @staticmethod
    def request_generator(get_data_barcode_v1_get_item_price_from_barcode_response):
        req_gen = []
        env = get_data_barcode_v1_get_item_price_from_barcode_response['Env']
        barcode = get_data_barcode_v1_get_item_price_from_barcode_response['Barcode']
        temp_api_url = get_data_barcode_v1_get_item_price_from_barcode_response['ApiUrl']
        api_url = temp_api_url.replace("<env>", env)
        api_url = api_url.replace("<barcode>", barcode)
        api_key = ConfigReader.read_config()['barcodev1_apikey']
        api_auth = ConfigReader.read_config()['barcodev1_apiauth']
        requestUrl = api_url + api_key
        params = {"Authorization": api_auth}
        req_gen.extend([requestUrl, params, barcode, api_url])
        return req_gen

    @pytest.fixture(params=DataReader.get_test_data("BarcodeV1_001"))
    def get_data_barcode_v1_get_item_price_from_barcode_response(self, request):
        return request.param
