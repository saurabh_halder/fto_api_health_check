##### Customer V2 Api Tests #####

from helpers.Logger import Logger
from testdata.DataReader import DataReader
from helpers.ConfigReader import ConfigReader
import pytest
import requests
import json


class TestCustomerV2(Logger):

    # Identity V1 Response Code Test
    def test_customer_v2_token_response(self, get_data_identity_token_response, get_data_customer_v2_user_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: POST/identity/v1/users")
        log.info("Attempting Login using Email: " + get_data_identity_token_response["Username"])
        payload = TestCustomerV2.request_generator_identity_token(get_data_identity_token_response)
        log.info("Api Request URL: " + payload[2])
        response = requests.post(payload[0], data=payload[1])
        log.info("Api Post Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        token = json.loads(response.text)['access_token']
        assert token, "Bearer Token received is Null"
        log.info("Bearer Token Received")
        log.info("End Point under Test: GET//customer/v2/customers/@me")
        apirequest = TestCustomerV2.request_generator_identity_user(get_data_customer_v2_user_response, token)
        log.info("Api Request URL: " + apirequest[2])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        log.info("Customer Details Received")
        response_json = json.loads(response.text)
        assert response_json, "JSON Response is Null"
        assert response_json['customerNumber'] == apirequest[3], "Salesforce Customer Number Incorrect"
        log.info("Customer Number in JSON Response is correct")
        assert response_json['email'] == payload[3], "Customer Email Incorrect"
        log.info("Customer Email in JSON Response is correct")
        assert response_json["firstName"], "First Name is Null"
        assert response_json["lastName"], "Last Name is Null"
        assert response_json["address1"], "Address 1 is Null"
        assert response_json["address2"], "Address 2 is Null"
        assert response_json["postcode"], "Postcode is Null"
        log.info("JSON Fields - First name, Last name, Address 1, Address 2 and Postcode are not empty")
        log.info("Test Completed")

    @staticmethod
    def request_generator_identity_token(get_data_identity_token_response):
        req_gen = []
        env = get_data_identity_token_response['Env']
        username = get_data_identity_token_response['Username']
        temp_api_url = get_data_identity_token_response['ApiUrl']
        api_url = temp_api_url.replace("<env>", env)
        api_key = ConfigReader.read_config()['identity_apikey']
        body = {'grant_type': 'password',
                'username': get_data_identity_token_response['Username'],
                'password': get_data_identity_token_response['Password']}
        requestUrl = api_url + api_key
        req_gen.extend([requestUrl, body, api_url, username])
        return req_gen

    @staticmethod
    def request_generator_identity_user(get_data_identity_user_response, token):
        req_gen = []
        env = get_data_identity_user_response['Env']
        customerNumber = get_data_identity_user_response['CustomerNumber']
        temp_api_url = get_data_identity_user_response['ApiUrl']
        api_url = temp_api_url.replace("<env>", env)
        api_key = ConfigReader.read_config()['identity_apikey']
        requestUrl = api_url + api_key
        params = {"Authorization": "Bearer " + token,
                  "Accept": "application/json"}
        req_gen.extend([requestUrl, params, api_url, customerNumber])
        return req_gen

    @pytest.fixture(params=DataReader.get_test_data("CustomerV2_001"))
    def get_data_identity_token_response(self, request):
        return request.param

    @pytest.fixture(params=DataReader.get_test_data("CustomerV2_002"))
    def get_data_customer_v2_user_response(self, request):
        return request.param
