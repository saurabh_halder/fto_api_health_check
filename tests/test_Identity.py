##### Identity V1 Api Tests #####

from helpers.Logger import Logger
from testdata.DataReader import DataReader
from helpers.ConfigReader import ConfigReader
import pytest
import requests
import json


class TestIdentity(Logger):

    # Identity V1 Response Code Test
    def test_identity_token_login_response(self, get_data_identity_token_response, get_data_identity_user_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: POST/identity/v1/users")
        log.info("Attempting Login using Email: " + get_data_identity_token_response["Username"])
        payload = TestIdentity.request_generator_identity_token(get_data_identity_token_response)
        log.info("Api Request URL: " + payload[2])
        response = requests.post(payload[0], data=payload[1])
        log.info("Api Post Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        token = json.loads(response.text)['access_token']
        assert token, "Bearer Token received is Null"
        log.info("Bearer Token Received")
        log.info("End Point under Test: GET/identity/v1/users/@me")
        apirequest = TestIdentity.request_generator_identity_user(get_data_identity_user_response, token)
        log.info("Api Request URL: " + apirequest[2])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        log.info("Login Successful")
        log.info("Test Completed")

    @staticmethod
    def request_generator_identity_token(get_data_identity_token_response):
        req_gen = []
        env = get_data_identity_token_response['Env']
        temp_api_url = get_data_identity_token_response['ApiUrl']
        api_url = temp_api_url.replace("<env>", env)
        api_key = ConfigReader.read_config()['identity_apikey']
        body = {'grant_type': 'password',
                'username': get_data_identity_token_response['Username'],
                'password': get_data_identity_token_response['Password']}
        requestUrl = api_url + api_key
        req_gen.extend([requestUrl, body, api_url])
        return req_gen

    @staticmethod
    def request_generator_identity_user(get_data_identity_user_response, token):
        req_gen = []
        env = get_data_identity_user_response['Env']
        temp_api_url = get_data_identity_user_response['ApiUrl']
        api_url = temp_api_url.replace("<env>", env)
        api_key = ConfigReader.read_config()['identity_apikey']
        requestUrl = api_url + api_key
        params = {"Authorization": "Bearer " + token,
                  "Accept": "application/json"}
        req_gen.extend([requestUrl, params, api_url])
        return req_gen

    @pytest.fixture(params=DataReader.get_test_data("Identity_001"))
    def get_data_identity_token_response(self, request):
        return request.param

    @pytest.fixture(params=DataReader.get_test_data("Identity_002"))
    def get_data_identity_user_response(self, request):
        return request.param
