import pytest


def pytest_html_report_title(report):
   report.title = "Food To Order Api Health Check Report"

@pytest.mark.hookwrapper
def pytest_runtest_makereport(item):
    """
        Extends the PyTest Plugin to take and embed screenshot in html report, whenever test fails.
        :param item:
        """
    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])

    if report.when == 'call' or report.when == "setup":
        html = '<div><alt="screenshot" style="width:300px;height:228px;"align="right"/></div>'
        extra.append(pytest_html.extras.html(html))
        report.extra = extra