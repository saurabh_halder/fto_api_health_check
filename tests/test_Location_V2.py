##### Location V2 Api Tests #####

from helpers.Logger import Logger
from testdata.DataReader import DataReader
from helpers.ConfigReader import ConfigReader
import pytest
import requests
import json


class TestLocationV2(Logger):

    # Location V2 Stores Response Code Test
    def test_location_v2_get_stores_response_code(self, get_data_get_stores_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: GET/location/v2/stores/")
        apirequest = TestLocationV2.request_generator(get_data_get_stores_response)
        log.info("Api Request URL: " + apirequest[2])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        log.info("Test Completed")

    # Location V2 Store Details Response Code Test
    def test_location_v2_get_stores_response_content(self, get_data_get_stores_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: GET/location/v2/stores/")
        apirequest = TestLocationV2.request_generator(get_data_get_stores_response)
        log.info("Api Request URL: " + apirequest[2])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        response_json = json.loads(response.text)
        assert response_json, "JSON Response is Null"
        log.info("JSON Response is not Null")
        log.info("Test Completed")

    # Location V2 Store Details Response Code Test
    def test_location_v2_get_store_details_response_code(self, get_data_get_store_details_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: GET/location/v2/stores/")
        apirequest = TestLocationV2.request_generator2(get_data_get_store_details_response)
        log.info("Api Request URL: " + apirequest[2])
        log.info("Testing with Store: " + apirequest[3])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        log.info("Test Completed")

    # Location V2 Store Details Response Content Test
    def test_location_v2_get_store_details_response_content(self, get_data_get_store_details_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: GET/location/v2/stores/")
        apirequest = TestLocationV2.request_generator2(get_data_get_store_details_response)
        log.info("Api Request URL: " + apirequest[2])
        log.info("Testing with Store: " + apirequest[3])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        response_json = json.loads(response.text)
        assert response_json, "JSON Response is Null"
        log.info("JSON Response is not Null")
        assert response_json["storeName"] == get_data_get_store_details_response['StoreName'], "Store Name Incorrect"
        assert response_json["category"] == get_data_get_store_details_response['StoreCategory'], "Store Category Incorrect"
        assert response_json["region"] == get_data_get_store_details_response['StoreRegion'], "Store Region Incorrect"
        assert response_json["address"]["city"] == get_data_get_store_details_response['StoreCity'], "Store City Incorrect"
        assert response_json["address"]["postcode"] == get_data_get_store_details_response['StorePO'], "Store Post Code Incorrect"
        assert response_json["address"]["country"] == get_data_get_store_details_response['StoreCountry'], "Store Country Incorrect"
        log.info("JSON Fields - Store Name, Category, Region, City, Post Code and Country validated")
        log.info("Test Completed")

    @staticmethod
    def request_generator(get_data_get_stores_response):
        req_gen = []
        env = get_data_get_stores_response['Env']
        temp_api_url = get_data_get_stores_response['ApiUrl']
        api_url = temp_api_url.replace("<env>", env)
        api_key = ConfigReader.read_config()['locationv2_apikey']
        api_auth = ConfigReader.read_config()['locationv2_apiauth']
        requestUrl = api_url + api_key
        params = {"Authorization": api_auth}
        req_gen.extend([requestUrl, params, api_url])
        return req_gen

    @staticmethod
    def request_generator2(get_data_get_store_details_response):
        req_gen = []
        env = get_data_get_store_details_response['Env']
        StoreId = get_data_get_store_details_response['StoreId']
        temp_api_url = get_data_get_store_details_response['ApiUrl']
        api_url = temp_api_url.replace("<env>", env)
        api_url = api_url.replace("<store>", StoreId)
        api_key = ConfigReader.read_config()['locationv2_apikey']
        api_auth = ConfigReader.read_config()['locationv2_apiauth']
        requestUrl = api_url + api_key
        params = {"Authorization": api_auth}
        req_gen.extend([requestUrl, params, api_url, StoreId])
        return req_gen

    @pytest.fixture(params=DataReader.get_test_data("LocationV2_001"))
    def get_data_get_stores_response(self, request):
        return request.param

    @pytest.fixture(params=DataReader.get_test_data("LocationV2_002"))
    def get_data_get_store_details_response(self, request):
        return request.param
