##### Promotion V2 Api Tests #####

from helpers.Logger import Logger
from testdata.DataReader import DataReader
from helpers.ConfigReader import ConfigReader
import pytest
import requests


class TestPromoV2(Logger):

    # Promotion V2 Response Code Test
    def test_promo_v2_get_item_promo_response(self, get_data_promo_v2_get_item_promo_response):
        log = self.get_logger()
        log.info("Test Started")
        log.info("End Point under Test: GET/promotion/v2/promotions?productid={Product}&storeId={StoreId}")
        apirequest = TestPromoV2.request_generator(get_data_promo_v2_get_item_promo_response)
        log.info("Api Request URL: " + apirequest[4])
        log.info("Testing with MIN: " + apirequest[2] + " and Store Id: " + apirequest[3])
        response = requests.get(apirequest[0], headers=apirequest[1])
        log.info("Api Get Response Received")
        assert response.status_code == 200, ("Response Code Received:", response.status_code, "Test Failed")
        log.info("Response Code received is 200")
        log.info("Test Completed")

    @staticmethod
    def request_generator(get_data_promo_v2_get_item_promo_response):
        req_gen = []
        env = get_data_promo_v2_get_item_promo_response['Env']
        ProductId = get_data_promo_v2_get_item_promo_response['ProductId']
        StoreId = get_data_promo_v2_get_item_promo_response['StoreId']
        temp_api_url = get_data_promo_v2_get_item_promo_response['ApiUrl']
        api_url = temp_api_url.replace("<env>", env)
        api_url = api_url.replace("<MIN>", ProductId)
        api_url = api_url.replace("<store>", StoreId)
        api_key = ConfigReader.read_config()['productv1_apikey']
        api_auth = ConfigReader.read_config()['productv1_apiauth']
        requestUrl = api_url + api_key
        params = {"Authorization": api_auth}
        req_gen.extend([requestUrl, params, ProductId, StoreId, api_url])
        return req_gen

    @pytest.fixture(params=DataReader.get_test_data("PromoV2_001"))
    def get_data_promo_v2_get_item_promo_response(self, request):
        return request.param
